# newsnow-load-test

## How to run locally

### Install dependencies

```
npm i
```
Then

```
npm i -g element
```

### Run the test

#### Enter credentials for newsnow

Under `unmodulized` folder, create `monza-prod.csv` for production, or `monza-UAT.csv` for UAT
In the `.csv` file, enter your credentials in the following format and save
```
USERNAME,PASSWORD
<Username>,<Password>
```

#### Run command

Now run the test with
```
element run unmodulized/flood-prod.ts
```
for production

OR with
```
element run unmodulized/flood-UAT.ts
```
for UAT

#### Non-headless mode

To run it in non-headless mode, add `--no-headless` after the command

### Upload to Flood.io

In flood.io, click "Streams", then "Create Stream".

Upload the `.ts` file under `unmodulized` folder along with its `.csv` credential file.

Choose 'Element' then you can configure the launch