import { step, TestSettings, TestData, Until, By, Browser } from '@flood/element'
// import moment from 'moment';

const loginPage = {
    "emailField": "form input[name=email]",
    "passwordField": "form input[name=password]",
    "loginButton": ".login-button"
};

const sideNav = {
	"pageButton": ".primary-nav li:nth-child(4) a:nth-child(2)",
    "publicationSelect": ".nn-publication-select a",
    "publicationSearchBox": ".select2-search input",
    "monthYear": ".quickdate-month",
    "nextMonthButton": ".quickdate-next-month",
    "prevMonthButton": ".quickdate-prev-month",
    "pageListContainer": ".page-list",
    "pageList": ".page-list li",
    "pubSearchResult": ".ui-select-choices-group span"
};

const infoPanel = {
    "infoPanelContainer": "#container",
    "mediaList": ".media-body span",
    "searchBox": ".searchBox",
    "searchResult": ".media-list div.media-item",
    "firstSearchResult": ".media-list div:nth-child(1)",
    "assignButton": ".taskFooter button:nth-child(1)",
    "removeTaskLink": ".removeTask",
    "closeCurrentTab": ".page-element-header i",
    "confirmButton": ".comfirm-message .primary",
    "assigneeButton": ".media-body .select2",
    "assigneeSearchBox": ".list-group-item .select2-search input",
    "assigneeSearchResult": ".select12-results span",
    "storyDesc": ".media-body textarea",
    "searchStoryLink": ".assignStory span a:nth-child(2)",
    "updateButton": ".taskFooter button:nth-child(1)",
    "copyFitList": ".page-element-item-validation .list-group div:nth-child(2) .page-element-detail",
    "copyFitLeft": "i.pull-left",
    "copyFitRight": "span.page-element-validation",
    "downloadPdfButton": "div.fa-title i.fa-download",
    "notificationCloseButton": "button.notification-close-button"
};

const baseUrl = "https://uat3.dev.newsnow.io/"

interface UserData {
    USERNAME: string,
    PASSWORD: string
}

TestData.fromCSV<UserData>('monza-UAT.csv').circular()

export const settings: TestSettings = {
	// userAgent: 'flood-chrome-test',
	// loopCount: 1,

	// Automatically wait for elements before trying to interact with them
    actionDelay: 7,
    stepDelay: 7,
	waitUntil: 'visible',
	waitTimeout: 60
}

export default () => {
	let login: Login
	let sideNavEl: SideNav
	let infoPanelEl: InfoPanel

	step('Setup', async browser => {
		// visit instructs the browser to launch, open a page, and navigate to https://newsnow.io/
		login = await new Login(browser);
		sideNavEl = await new SideNav(browser);
		infoPanelEl = await new InfoPanel(browser);
	})

	step('Login', async (browser, data: UserData) => {
		// visit instructs the browser to launch, open a page, and navigate to https://newsnow.io/
		await browser.visit(baseUrl)
		// await browser.takeScreenshot()
		const monzaUser = data.USERNAME
		const monzaPassword = data.PASSWORD
		console.log(monzaUser)
		await login.login(monzaUser!, monzaPassword!)
	})

	step('Open the book', async browser => {
		// visit instructs the browser to launch, open a page, and navigate to https://newsnow.io/
		await sideNavEl.openBook('The Canberra Times', "2020-04-28")
	})

	step('Open a random page', async browser => {
		// visit instructs the browser to launch, open a page, and navigate to https://newsnow.io/
		let pageNumber = await sideNavEl.openRandomPage()
		console.log(`Test Page: ${pageNumber + 1}`)
	})

	step('Assign furnitures', async browser => {
		// visit instructs the browser to launch, open a page, and navigate to https://newsnow.io/
		await infoPanelEl.assignFurnitures()
    })
    
	step('Assign stories', async browser => {
		// visit instructs the browser to launch, open a page, and navigate to https://newsnow.io/
		await infoPanelEl.assignStories()
	})
	// // browser keyword can be shorthanded as "b" or anything that is descriptive to you.
	// step('Step 2 find first heading', async b => {
	// 	// Note the use of await here, this is the async/await pattern https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Asynchronous/Async_await
	// 	// and is required everytime you talk to the browser
	// 	const firstHeading = await b.findElement(By.css('h1,h2,h3,p'))
	// 	await b.highlightElement(firstHeading)
	// 	const h1Text = await firstHeading.text()

	// 	// You can use console.log to write out to the command line
	// 	console.log(h1Text)
	// })
}

class Login {
    b: Browser
    constructor(b: Browser) {
        this.b = b;
    }

    login = async (email: string, password: string) => {
        await this.b.wait(Until.elementIsVisible(loginPage.emailField))
        await this.b.type(By.css(loginPage.emailField), email, {delay: 1})
        await this.b.type(By.css(loginPage.passwordField), password, {delay: 1})

        await this.b.wait(Until.elementIsVisible(loginPage.loginButton))
        await this.b.click(By.css(loginPage.loginButton))
        await this.b.wait(2)
    }
}

class SideNav {
    b: Browser
    constructor(b: Browser) {
        this.b = b;
    }

    openBook = async (site: string, date = "2025-12-31") => {
        await this.b.wait(Until.elementIsVisible(sideNav.pageButton))
        await this.b.click(By.css(sideNav.pageButton))
        await this.b.wait(0.1)
        await this.b.wait(Until.elementIsVisible(sideNav.publicationSelect))
        await this.b.click(By.css(sideNav.publicationSelect))

        await this.b.wait(Until.elementIsVisible(sideNav.publicationSearchBox))
        await this.b.type(By.css(sideNav.publicationSearchBox), site, {delay: 1})
        // await this.b.wait(1)
        await this.b.click(By.css(sideNav.pubSearchResult))
        // await this.b.press('Enter')
        await this.b.visit(`${baseUrl}pages/CT/${date}`)
        await this.b.wait(Until.elementIsVisible(sideNav.pageListContainer))
    }

    openRandomPage = async () => {
        let pageList = await this.b.findElements(By.css(sideNav.pageList))
        let flag = 0
        for (let index = 0; index < pageList.length; index++) {
            const pageEl = pageList[index];
            const pageNumberEl = await pageEl.findElement(By.css('small'))! as ElementHandle
            let pageNumberText = await pageNumberEl.text()! as string
            pageNumberText = pageNumberText === 'Cover' ? "1" : pageNumberText
            let pageNumber: number
            pageNumber = parseInt(pageNumberText, 10)
            if (pageNumber > flag) {
                flag = pageNumber
            } else {
                pageList = pageList.slice(0, index)
                break;
            }
        }
        let randomPageNumber = Math.floor(Math.random() * Math.floor(pageList.length)) - 1
        randomPageNumber = randomPageNumber < 0 ? 0 : randomPageNumber
        const randomPageEl = pageList[randomPageNumber]! as ElementHandle;
        await randomPageEl.click()
        await this.b.wait(Until.elementIsVisible(infoPanel.infoPanelContainer))
        await this.b.wait(0.2)

        return randomPageNumber + 1

        // await this.b.focus(scrollable_section);
    }
}



class InfoPanel {
    b: Browser
    constructor(b: Browser) {
        this.b = b;
    }

    assignFurnitures = async () => {

        for (let index = 1; index < 5; index++) {
            await this.b.wait(2)
            let furnitureSize = await this.b.page.evaluate(() => {
                let a = Array.prototype.map.call(document.querySelectorAll(".media-body span"), (el) => el.textContent)
                return a.filter(el => el.includes("Furniture")).length
            })

            if(furnitureSize >= index){
                await this.b.page.evaluate((index) => {
                    let elements = document.querySelectorAll(".media-body span")
                    for (const el of elements) {
                        if(el.textContent.includes(`Furniture ${index}`)){
                            el.click()
                            break;
                        }
                    }
                },index)

                await this.b.wait(1)
                // if (await this.b.findElement(By.css(infoPanel.removeTaskLink)) !== null){
                //     console.log("remove")
                //     this.b.click(By.css(infoPanel.removeTaskLink))
                //     await this.b.wait(Until.elementIsVisible(infoPanel.confirmButton))
                //     await this.b.click(By.css(infoPanel.confirmButton))
                // }
                try {
                    await this.b.page.waitForSelector(infoPanel.removeTaskLink, {timeout: 2000})
                    await this.b.page.click(infoPanel.removeTaskLink)
                    await this.b.page.waitForSelector(infoPanel.confirmButton)
                    await this.b.page.click(infoPanel.confirmButton)
                } catch (error) {
                }

                await this.b.wait(Until.elementIsVisible(infoPanel.searchBox))
                await this.b.wait(0.2)
                await this.b.type(By.css(infoPanel.searchBox), `dummy test ${index}`)
                await this.b.wait(1)

                // await this.page.keyboard.press('Enter')
                await this.b.wait(Until.elementIsVisible(infoPanel.searchResult))
                await this.b.page.waitFor(selector => document.querySelectorAll(selector).length === 1, {}, infoPanel.searchResult)

                await this.b.click(By.css(infoPanel.firstSearchResult))
                await this.b.wait(Until.elementIsVisible(infoPanel.assignButton))

                await this.b.click(By.css(infoPanel.assignButton))
                await this.b.wait(2)

                await this.b.wait(Until.elementIsVisible(infoPanel.removeTaskLink))
                await this.b.click(By.css(infoPanel.closeCurrentTab))
                await this.b.wait(0.5)

                await this.b.wait(Until.elementIsVisible(infoPanel.infoPanelContainer))
            }
            else {
                break;
            }

        }
        
    }

    assignStories = async () => {
        for (let index = 1; index < 5; index++) {
            let storySize = await this.b.page.evaluate(() => {
                let a = Array.prototype.map.call(document.querySelectorAll(".media-body span"), (el) => el.textContent)
                return a.filter(el => el.includes("Stories")).length
            })
            if(storySize >= index){
                await this.b.page.evaluate((index) => {
                    let elements = document.querySelectorAll(".media-body span")
                    for (const el of elements) {
                        if(el.textContent.includes(`Stories ${index}`)){
                            el.click()
                            break;
                        }
                    }
                },index)
                await this.b.wait(0.2)
                try {
                    await this.b.findElement(By.css(infoPanel.removeTaskLink))
                    this.b.click(By.css(infoPanel.removeTaskLink))
                    await this.b.wait(Until.elementIsVisible(infoPanel.confirmButton))
                    await this.b.click(By.css(infoPanel.confirmButton))
                } catch(err) {
                    
                }
                await this.b.wait(0.5)

                await this.b.wait(Until.elementIsVisible(infoPanel.assigneeButton))
                await this.b.click(By.css(infoPanel.assigneeButton))
                console.log('wait3')

                await this.b.wait(Until.elementIsVisible(infoPanel.assigneeSearchBox))

                await this.b.page.$(infoPanel.assigneeSearchBox)
                // await assigneeSearchBox.type("ACM user 3", {delay: 0.1})
                await this.b.type(By.css(infoPanel.assigneeSearchBox), "Sinclair Liang", {delay: 0.1})
                await this.b.wait(0.3)
                
                await this.b.page.evaluate(() => {
                    var a = document.querySelectorAll('.select2-highlighted span')[1]
                    a.click()
                })
                // await this.b.click(By.partialLinkText("ACM user 3"))

                await this.b.wait(Until.elementIsVisible(infoPanel.storyDesc))
                await this.b.type(By.css(infoPanel.storyDesc),"test")

                await this.b.wait(Until.elementIsVisible(infoPanel.assignButton))
                await this.b.click(By.css(infoPanel.assignButton))

                await this.b.wait(2)

                await this.b.wait(Until.elementIsVisible(infoPanel.searchStoryLink))
                await this.b.click(By.css(infoPanel.searchStoryLink))
                await this.b.wait(0.2)

                await this.b.wait(Until.elementIsVisible(infoPanel.searchBox))
                await this.b.type(By.css(infoPanel.searchBox), `T${index}hd`, {delay: 0.1})

                // await this.page.keyboard.press('Enter')
                await this.b.wait(Until.elementIsVisible(infoPanel.searchResult))
                await this.b.page.waitFor(selector => document.querySelectorAll(selector).length === 1, {}, infoPanel.searchResult)

                await this.b.click(By.css(infoPanel.firstSearchResult))
                await this.b.wait(0.5)

                try {
                    await this.b.page.waitForSelector(infoPanel.confirmButton, {timeout: 3000})
                    await this.b.page.click(infoPanel.confirmButton)
                } catch (error) {
                    // console.log(error)
                }

                try {
                    await this.b.page.waitForSelector(infoPanel.notificationCloseButton, {timeout: 1000})
                    await this.b.page.click(infoPanel.notificationCloseButton)
                } catch (error) {
                    // console.log(error)
                }

                await this.b.wait(1.5)
                

                await this.b.wait(Until.elementIsVisible(infoPanel.updateButton))
                await this.b.click(By.css(infoPanel.updateButton))
                try {
                    await this.b.page.waitForSelector(infoPanel.notificationCloseButton, {timeout: 1000})
                    await this.b.page.click(infoPanel.notificationCloseButton)
                } catch (error) {
                    console.log(error)
                }
                await this.b.wait(7)
                await this.b.wait(1)
                // await this.b.page.waitFor((notificationBox) => {
                //     return document.querySelectorAll(notificationBox).length < 1
                // }, {}, infoPanel.notificationBox)
                await this.b.click(By.css(infoPanel.closeCurrentTab))
                await this.b.wait(Until.elementIsVisible(infoPanel.infoPanelContainer))
                await this.b.wait(1)
            }
            else {
                break;
            }

        }
    }
}