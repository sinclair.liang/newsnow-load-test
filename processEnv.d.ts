export {}

declare global {
	namespace NodeJS {
		interface ProcessEnv {
            MONZA_TEST_EMAIL: string;
            MONZA_TEST_PASSWORD: string;
		}
	}
}
