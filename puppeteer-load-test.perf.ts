import { step, TestSettings } from '@flood/element'
import Login from './functions/login';
import SideNav from './functions/sideNav';
import InfoPanel from './functions/infoPanel';

const monzaUser = "sinclair.liang+test@fairfaxmedia.com.au"
const monzaPassword = "@Acm1943"

const baseUrl = "https://newsnow.io/"
export const settings: TestSettings = {
	// userAgent: 'flood-chrome-test',
	// loopCount: 1,

	// Automatically wait for elements before trying to interact with them
	waitUntil: 'visible',
	waitTimeout: 20
}

export default () => {
	let login: Login
	let sideNav: SideNav
	let infoPanel: InfoPanel

	step('Setup', async browser => {
		// visit instructs the browser to launch, open a page, and navigate to https://newsnow.io/
		login = await new Login(browser);
		sideNav = await new SideNav(browser);
		infoPanel = await new InfoPanel(browser);
	})

	step('Start', async browser => {
		// visit instructs the browser to launch, open a page, and navigate to https://newsnow.io/
		await browser.visit(baseUrl)
		// await browser.takeScreenshot()
		console.log(monzaUser)
		await login.login(monzaUser!, monzaPassword!)
	})

	step('Open the book', async browser => {
		// visit instructs the browser to launch, open a page, and navigate to https://newsnow.io/
		await sideNav.openBook('AUTO', "2025-12-31")
	})

	step('Open a random page', async browser => {
		// visit instructs the browser to launch, open a page, and navigate to https://newsnow.io/
		let pageNumber = await sideNav.openRandomPage()
		console.log(`Test Page: ${pageNumber + 1}`)
	})

	step('Assign furnitures', async browser => {
		// visit instructs the browser to launch, open a page, and navigate to https://newsnow.io/
		await infoPanel.assignFurnitures()
	})
	step('Assign stories', async browser => {
		// visit instructs the browser to launch, open a page, and navigate to https://newsnow.io/
		await infoPanel.assignStories()
	})
	// // browser keyword can be shorthanded as "b" or anything that is descriptive to you.
	// step('Step 2 find first heading', async b => {
	// 	// Note the use of await here, this is the async/await pattern https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Asynchronous/Async_await
	// 	// and is required everytime you talk to the browser
	// 	const firstHeading = await b.findElement(By.css('h1,h2,h3,p'))
	// 	await b.highlightElement(firstHeading)
	// 	const h1Text = await firstHeading.text()

	// 	// You can use console.log to write out to the command line
	// 	console.log(h1Text)
	// })
}

