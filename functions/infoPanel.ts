import infoPanel from '../pageObjects/infoPanel.json'
import { Until,By } from '@flood/element'
import { Browser } from '@flood/element';

export default class InfoPanel {
    b: Browser
    constructor(b: Browser) {
        this.b = b;
    }

    assignFurnitures = async () => {
        for (let index = 1; index < 5; index++) {
            let furnitureSize = await this.b.page.evaluate(() => {
                let a = Array.prototype.map.call(document.querySelectorAll(".media-body span"), (el) => el.textContent)
                return a.filter(el => el.includes("Furniture")).length
            })
            if(furnitureSize >= index){
                await this.b.page.evaluate((index) => {
                    let elements = document.querySelectorAll(".media-body span")
                    for (const el of elements) {
                        if(el.textContent.includes(`Furniture ${index}`)){
                            el.click()
                            break;
                        }
                    }
                },index)
                await this.b.wait(0.2)
                if (await this.b.findElement(By.css(infoPanel.removeTaskLink)) !== null){
                    this.b.click(By.css(infoPanel.removeTaskLink))
                    await this.b.wait(Until.elementIsVisible(infoPanel.confirmButton))
                    await this.b.click(By.css(infoPanel.confirmButton))
                }
                await this.b.wait(Until.elementIsVisible(infoPanel.searchBox))
                await this.b.wait(0.2)

                await this.b.type(By.css(infoPanel.searchBox), `dummy test ${index}`)
                await this.b.wait(0.2)

                // await this.page.keyboard.press('Enter')
                await this.b.wait(Until.elementIsVisible(infoPanel.searchResult))
                await this.b.page.waitFor(selector => document.querySelectorAll(selector).length === 1, {}, infoPanel.searchResult)

                await this.b.click(By.css(infoPanel.firstSearchResult))
                await this.b.wait(Until.elementIsVisible(infoPanel.assignButton))

                await this.b.click(By.css(infoPanel.assignButton))
                await this.b.wait(2)

                await this.b.wait(Until.elementIsVisible(infoPanel.removeTaskLink))
                await this.b.click(By.css(infoPanel.closeCurrentTab))
                await this.b.wait(0.5)

                await this.b.wait(Until.elementIsVisible(infoPanel.infoPanelContainer))
            }
            else {
                break;
            }

        }
        
    }

    assignStories = async () => {
        for (let index = 1; index < 5; index++) {
            let storySize = await this.b.page.evaluate(() => {
                let a = Array.prototype.map.call(document.querySelectorAll(".media-body span"), (el) => el.textContent)
                return a.filter(el => el.includes("Stories")).length
            })
            if(storySize >= index){
                await this.b.page.evaluate((index) => {
                    let elements = document.querySelectorAll(".media-body span")
                    for (const el of elements) {
                        if(el.textContent.includes(`Stories ${index}`)){
                            el.click()
                            break;
                        }
                    }
                },index)
                await this.b.wait(0.2)
                if (await this.b.findElement(By.css(infoPanel.removeTaskLink)) !== null){
                    this.b.click(By.css(infoPanel.removeTaskLink))
                    await this.b.wait(Until.elementIsVisible(infoPanel.confirmButton))
                    await this.b.click(By.css(infoPanel.confirmButton))
                }
                await this.b.wait(0.5)

                await this.b.wait(Until.elementIsVisible(infoPanel.assigneeButton))
                await this.b.click(By.css(infoPanel.assigneeButton))

                await this.b.wait(Until.elementIsVisible(infoPanel.assigneeSearchBox))

                await this.b.page.$(infoPanel.assigneeSearchBox)
                // await assigneeSearchBox.type("ACM user 3", {delay: 0.1})
                await this.b.type(By.css(infoPanel.assigneeSearchBox), "ACM user 3", {delay: 0.1})
                await this.b.wait(0.3)
                
                await this.b.page.evaluate(() => {
                    var a = document.querySelectorAll('.select2-highlighted span')[1]
                    a.click()
                })
                // await this.b.click(By.partialLinkText("ACM user 3"))

                await this.b.wait(Until.elementIsVisible(infoPanel.storyDesc))
                await this.b.type(By.css(infoPanel.storyDesc),"test")

                await this.b.wait(Until.elementIsVisible(infoPanel.assignButton))
                await this.b.click(By.css(infoPanel.assignButton))

                await this.b.wait(2)

                await this.b.wait(Until.elementIsVisible(infoPanel.searchStoryLink))
                await this.b.click(By.css(infoPanel.searchStoryLink))
                await this.b.wait(0.2)

                await this.b.wait(Until.elementIsVisible(infoPanel.searchBox))
                await this.b.type(By.css(infoPanel.searchBox), `T${index}Heading Dummy Story`, {delay: 0.1})

                // await this.page.keyboard.press('Enter')
                await this.b.wait(Until.elementIsVisible(infoPanel.searchResult))
                await this.b.page.waitFor(selector => document.querySelectorAll(selector).length === 1, {}, infoPanel.searchResult)

                await this.b.click(By.css(infoPanel.firstSearchResult))
                await this.b.wait(0.5)

                try {
                    await this.b.page.waitForSelector(infoPanel.confirmButton, {timeout: 3000})
                    await this.b.page.click(infoPanel.confirmButton)
                } catch (error) {
                    // console.log(error)
                }

                try {
                    await this.b.page.waitForSelector(infoPanel.notificationCloseButton, {timeout: 1000})
                    await this.b.page.click(infoPanel.notificationCloseButton)
                } catch (error) {
                    // console.log(error)
                }

                await this.b.wait(1.5)
                

                await this.b.wait(Until.elementIsVisible(infoPanel.updateButton))
                await this.b.click(By.css(infoPanel.updateButton))
                try {
                    await this.b.page.waitForSelector(infoPanel.notificationCloseButton, {timeout: 1000})
                    await this.b.page.click(infoPanel.notificationCloseButton)
                } catch (error) {
                    console.log(error)
                }
                await this.b.wait(7)
                await this.b.wait(1)
                // await this.b.page.waitFor((notificationBox) => {
                //     return document.querySelectorAll(notificationBox).length < 1
                // }, {}, infoPanel.notificationBox)
                await this.b.click(By.css(infoPanel.closeCurrentTab))
                await this.b.wait(Until.elementIsVisible(infoPanel.infoPanelContainer))
                await this.b.wait(1)
            }
            else {
                break;
            }

        }
    }
}