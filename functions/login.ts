import loginPage from '../pageObjects/login.json'
import { Until,By } from '@flood/element'
import { Browser } from '@flood/element';

export default class Login {
    b: Browser
    constructor(b: Browser) {
        this.b = b;
    }

    login = async (email: string, password: string) => {
        await this.b.wait(Until.elementIsVisible(loginPage.emailField))
        await this.b.type(By.css(loginPage.emailField), email, {delay: 1})
        await this.b.type(By.css(loginPage.passwordField), password, {delay: 1})

        await this.b.wait(Until.elementIsVisible(loginPage.loginButton))
        await this.b.click(By.css(loginPage.loginButton))
        await this.b.wait(2)
    }
}