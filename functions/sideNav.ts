import sideNav from '../pageObjects/sideNav.json'
import infoPanel from '../pageObjects/infoPanel.json'
import moment from 'moment';
// import * as fs from 'fs';
// import * as path from 'path';
import { Until,By } from '@flood/element'
import { Browser, ElementHandle } from '@flood/element';

export default class SideNav {
    b: Browser
    constructor(b: Browser) {
        this.b = b;
    }

    openBook = async (site: string, date = "2020-03-01") => {
        await this.b.wait(Until.elementIsVisible(sideNav.pageButton))
        await this.b.click(By.css(sideNav.pageButton))
        await this.b.wait(0.1)
        await this.b.wait(Until.elementIsVisible(sideNav.publicationSelect))
        await this.b.click(By.css(sideNav.publicationSelect))

        await this.b.wait(Until.elementIsVisible(sideNav.publicationSearchBox))
        console.log(`\n\n\nAAAA\n\n\n`)
        await this.b.type(By.css(sideNav.publicationSearchBox), site, {delay: 1})
        console.log(`\n\n\nBBBB\n\n\n`)
        // await this.b.wait(1)
        await this.b.click(By.css(sideNav.pubSearchResult))
        // await this.b.press('Enter')
        console.log(`\n\n\nCCCC\n\n\n`)

        await this.b.wait(0.1)

        let myDay = date.split('-')[2]
        let month = date.split('-')[1]
        let year = date.split('-')[0]
        let flag = 0;
        while(flag !== 1){
            let monthYearElement = await this.b.findElement(By.css(sideNav.monthYear))
            let monthYearTextOnPage = await monthYearElement.text()
            let monthYearOnPage = moment(monthYearTextOnPage, 'MMMM YYYY')
            // let monthYearDateOnPage = monthYearOnPage.toDate()
            
            let monthYearChosen = moment(month + '/' + year, 'MM/YYYY')
            // let monthYearDateChosen = monthYearChosen.toDate()

            if(monthYearOnPage.isBefore(monthYearChosen) === true){
                await this.b.click(By.css(sideNav.nextMonthButton))
            } else if(monthYearOnPage.isAfter(monthYearChosen) === true){
                await this.b.click(By.css(sideNav.prevMonthButton))
            } else {
                flag = 1
            }
        }
        await this.b.wait(0.3)
        let days = await this.b.findElements(By.css('tr td'))
        for (const day of days) {
            const dayText = await day.text()
            if(dayText === myDay){
                const dayClassName = await day.getProperty('className')
                // let dayClass = await dayClassName.jsonValue()
                let dayClass = dayClassName!
                if(!dayClass.includes('other-month')){
                    await day.click()
                    break;
                }
            }
        }
        await this.b.wait(Until.elementIsVisible(sideNav.pageListContainer))
    }

    openRandomPage = async () => {
        let pageList = await this.b.findElements(By.css(sideNav.pageList))
        let flag = 0
        for (let index = 0; index < pageList.length; index++) {
            const pageEl = pageList[index];
            const pageNumberEl = await pageEl.findElement(By.css('small'))! as ElementHandle
            let pageNumberText = await pageNumberEl.text()! as string
            pageNumberText = pageNumberText === 'Cover' ? "1" : pageNumberText
            let pageNumber: number
            pageNumber = parseInt(pageNumberText, 10)
            if (pageNumber > flag) {
                flag = pageNumber
            } else {
                pageList = pageList.slice(0, index)
                break;
            }
        }
        let randomPageNumber = Math.floor(Math.random() * Math.floor(pageList.length)) - 1
        randomPageNumber = randomPageNumber < 0 ? 0 : randomPageNumber
        const randomPageEl = pageList[randomPageNumber]! as ElementHandle;
        await randomPageEl.click()
        await this.b.wait(Until.elementIsVisible(infoPanel.infoPanelContainer))
        await this.b.wait(0.2)

        return randomPageNumber + 1

        // await this.b.focus(scrollable_section);
    }
}